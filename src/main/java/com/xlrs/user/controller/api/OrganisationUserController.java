package com.xlrs.user.controller.api;

import static com.xlrs.security.constant.UserRoles.ROLE_ADMIN;
import static com.xlrs.security.constant.UserRoles.ROLE_USER;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.user.service.OrganisationUserService;
import com.xlrs.user.view.OrganisationUserView;
import com.xlrs.user.view.PasswordView;
import com.xlrs.user.view.ResetPasswordView;
import com.xlrs.security.security.AuthorizeRoles;
import com.xlrs.security.util.ThreadAttribute;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user/orguser")
@Slf4j
public class OrganisationUserController {

	@Autowired
	private OrganisationUserService organisationUserService;

	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createOrgUser(@RequestBody OrganisationUserView orgUserView) throws Exception {
		log.debug("Requested payload is : " + orgUserView);

		try {
			return new ResponseView(organisationUserService.createOrgUser(orgUserView));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}

	@GetMapping("/{id}")
	public ResponseView getOrgUser(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(organisationUserService.getOrgUser(id));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}

	@GetMapping("/{id}/detail")
	public ResponseView getUserAndOrgDetail(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(organisationUserService.getUserAndOrgDetail(id));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}

	@RequestMapping(value = "/deactivate", method = RequestMethod.PUT, consumes = { "application/json" }, produces = {
			"application/json" })
	@AuthorizeRoles(values = { ROLE_ADMIN, ROLE_USER })
	public ResponseView deActivateUser(@RequestParam("orgUserId") Long orgUserId) throws Exception {
		try {
			organisationUserService.deActivateUser(orgUserId);
			return new ResponseView(messages.getMessage("user.status.change.success", null, null));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null), e.getMessage(),e);
		}
	}

	@RequestMapping(value = "/delete", method = RequestMethod.PUT, consumes = { "application/json" }, produces = {
			"application/json" })
	@AuthorizeRoles(values = {ROLE_ADMIN})
	public ResponseView deleteUser(@RequestParam("orgUserId") long orgUserId) throws Exception {
		try {
			organisationUserService.deleteUser(orgUserId);
			return new ResponseView(messages.getMessage("user.status.change.success", null, null));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null), e.getMessage(),e);
		}
	}

	@RequestMapping(value = "/activate", method = RequestMethod.PUT, consumes = { "application/json" }, produces = {
			"application/json" })
	@AuthorizeRoles(values = { ROLE_ADMIN, ROLE_USER })
	public ResponseView activateUser(@RequestParam("orgUserId") long orgUserId) throws Exception {
		try {
			organisationUserService.activateUser(orgUserId);
			return new ResponseView(messages.getMessage("user.status.change.success", null, null));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null), e.getMessage(), e);
		}
	}
	
	@GetMapping("/{username}/get")
	public ResponseView getOrgUserByUserName(@PathVariable String username) throws Exception {
		try {
			return new ResponseView(organisationUserService.getOrgUserByUserName(username));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@RequestMapping(value = "/sendResetPasswordMail", method = RequestMethod.POST)
	@ResponseBody
	public ResponseView sendResetPasswordMail(HttpServletRequest request, @RequestParam("username") final String username)
			throws  ApplicationException {
		try {
			organisationUserService.createPasswordResetToken(username);
			return new ResponseView(messages.getMessage("message.resendToken", null, null));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null),e.getMessage(), e);
		}
	}
	
	/**
	 * When user clicks on forget password link and update password with OTP and submit
	 * 
	 * @param passwordView
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	@ResponseBody
	public ResponseView resetPassword(HttpServletRequest request, @RequestBody ResetPasswordView view) throws ApplicationException {
		try {
			organisationUserService.resetPassword(view.getUsername(), view.getPassword(), view.getEmailOTPToken());
			return new ResponseView(messages.getMessage("message.resetPasswordSuc", null, null));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null),e.getMessage(), e);
		}
	}
	
	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	@ResponseBody
	public ResponseView updatePassword(@RequestBody PasswordView passwordView, HttpServletRequest request) throws ApplicationException {
		try {
			String username = ThreadAttribute.getUserInContext().getUsername();
			organisationUserService.changeUserPassword(username, passwordView.getNewPassword());
			return new ResponseView(messages.getMessage("message.updatePasswordSuc", null, null));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null),e.getMessage(), e);
		}
	}
	
	
}
