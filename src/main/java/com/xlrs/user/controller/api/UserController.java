package com.xlrs.user.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.user.service.UserService;
import com.xlrs.user.view.UserView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createUser(@RequestBody UserView createUserView) throws Exception {
		log.debug("Requested payload is : " + createUserView);
		
		try {
			return new ResponseView(userService.createUser(createUserView));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/{id}")
	public ResponseView getUser(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(userService.getUser(id));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/health")
	public String createUser() throws Exception {
		return "success";
	}
}
