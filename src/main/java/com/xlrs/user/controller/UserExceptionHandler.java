package com.xlrs.user.controller;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import com.xlrs.commons.controller.AbstractRestHandler;
import com.xlrs.commons.view.ErrorView;
import com.xlrs.commons.view.HTTPStatus;
import com.xlrs.commons.view.ResponseView;

/**
 * This class is meant to be extended by all REST resource "controllers". It
 * contains exception mapping and other common REST API functionality
 */
@ControllerAdvice
public class UserExceptionHandler extends AbstractRestHandler{

	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	@ExceptionHandler(UsernameNotFoundException.class)
	public @ResponseBody ResponseView handleUsernameNotFoundException(UsernameNotFoundException ex,
			WebRequest request) {
		return new ResponseView(HTTPStatus.PRECONDITION_FAILED, null,
				new ErrorView(HTTPStatus.PRECONDITION_FAILED, ex.getMessage()));
	}
}