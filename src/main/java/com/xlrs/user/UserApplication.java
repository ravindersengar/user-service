package com.xlrs.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
@PropertySource({
    "classpath:messages.properties"
})
@ComponentScan(basePackages = "com.xlrs")
public class UserApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(UserApplication.class, args);
	}
}

