package com.xlrs.user.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.user.feign.OrganisationProxy;
import com.xlrs.user.view.OrganisationView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class OrganisationRestTemplate {
	
	
//	@Autowired
//	private RestServiceBuilder<ResponseView> restServiceBuilder;
	
	@Autowired
	private OrganisationProxy orgProxy;

	
	@Value("${organisation.get.url}")
	private String getOrganisationURL;
	
	@Autowired
	private MessageSource messages;

	public OrganisationView getOrgById(Long orgSystemId) throws ApplicationException {
		OrganisationView organisationView =null;
		try {
			if(getOrganisationURL == null || "".equals(getOrganisationURL)) {
				throw new ApplicationException("URL found Null");
			}
			log.info("calling org service url ::::::::::::: " + getOrganisationURL);
//			ResponseView responseView = restServiceBuilder.get(getOrganisationURL, ResponseView.class, orgSystemId);
			ResponseView responseView = orgProxy.getOrganisation(orgSystemId);
			if(responseView.getErrorMessages()==null) {
				organisationView =  new ObjectMapper().convertValue(responseView.getData(), OrganisationView.class);
			}
			log.info("Getting organisation by organisation-id" + organisationView.toString());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.service.system.error.message", null, null), e.getMessage(),e);
		}
		return organisationView;
	}

}
