package com.xlrs.user.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.user.entity.Users;
import com.xlrs.user.repository.UserRepository;
import com.xlrs.user.view.UserView;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private MessageSource messages;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BaseRepository<Users> baseRepository;

	@Override
	public UserView createUser(UserView userView) throws NoResultFoundException {
		Users user = userView.getId() != null ? getUserById(userView.getId()) : new Users();
		user.setName(userView.getName());
		user.setId(userView.getId());
		user.setSsn(userView.getSsn());
		
		user = userRepository.save(baseRepository.addAuditFeilds(user, null, CommonConstants.STATUS_ACTIVE));
		return new UserView(user.getId(), user.getName(), user.getSsn());
	}

	@Override
	public UserView getUser(Long id) throws NoResultFoundException  {
		Users user = getUserById(id);
		return new UserView(user.getId(), user.getName(), user.getSsn());
	}

	public Users getUserById(Long id) throws NoResultFoundException {
		Optional<Users> optional = userRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		Users user = optional.get();
		return user;
	}

}
