package com.xlrs.user.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.util.CommonUtil;
import com.xlrs.commons.util.EncryptionUtil;
import com.xlrs.commons.view.EmailView;
import com.xlrs.security.exception.InvalidTokenException;
import com.xlrs.user.constant.UserConstants;
import com.xlrs.user.entity.OrganisationUser;
import com.xlrs.user.entity.PasswordResetToken;
import com.xlrs.user.entity.Users;
import com.xlrs.user.exception.PasswordNotMatchedException;
import com.xlrs.user.exception.UserNotFoundException;
import com.xlrs.user.feign.MailerProxy;
import com.xlrs.user.helper.UserNotificationHelper;
import com.xlrs.user.repository.OrganisationUserRepository;
import com.xlrs.user.repository.PasswordResetTokenRepository;
import com.xlrs.user.rest.OrganisationRestTemplate;
import com.xlrs.user.view.OrganisationUserView;
import com.xlrs.user.view.OrganisationView;

@Service
public class OrganisationUserServiceImpl implements OrganisationUserService {

	@Autowired
	private MessageSource messages;
	
	@Autowired
	private OrganisationUserRepository organisationUserRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BaseRepository<OrganisationUser> baseRepository;
	
	@Autowired
	private OrganisationRestTemplate organisationRestTemplate;
	
	@Autowired
	private PasswordResetTokenRepository passwordResetTokenRepository;
	
	@Autowired
	private UserNotificationHelper userNotificationHelper;
	
	@Autowired
	private MailerProxy mailerProxy;

	@Override
	public OrganisationUserView createOrgUser(OrganisationUserView organisationUserView) throws Exception {
		
		Users user = userService.getUserById(organisationUserView.getUserId());
		OrganisationUser organisationUser = organisationUserView.getId() != null ? getOrgUserById(organisationUserView.getId()) : new OrganisationUser();
		organisationUser.setOrganisationId(organisationUserView.getOrganisationId());
		organisationUser.setId(organisationUserView.getId());
		organisationUser.setPassword(EncryptionUtil.encrypt(organisationUserView.getPassword()));
		organisationUser.setUserName(organisationUserView.getUserName());
		organisationUser.setEmail(organisationUserView.getEmail());
		organisationUser.setUserRoles(organisationUserView.getUserRole());
		organisationUser.setName(user.getName());
		organisationUser.setUser(user);
		organisationUser = organisationUserRepository.save(baseRepository.addAuditFeilds(
				organisationUser, null, CommonConstants.STATUS_ACTIVE));
		if( organisationUserView.getId()  == null) {
			sendWelcomeMail(organisationUser);
		}
		
		return new OrganisationUserView(organisationUser.getId(), organisationUser.getOrganisationId(),organisationUser.getUser().getId(), 
				organisationUser.getUserName(), null, null, organisationUser.getUser().getName(), organisationUser.getEmail());
	}

	@Override
	public OrganisationUserView getOrgUser(Long id) throws NoResultFoundException {
		OrganisationUser orgUser = getOrgUserById(id);
		return new OrganisationUserView(orgUser.getId(), orgUser.getOrganisationId(), orgUser.getUser().getId(), orgUser.getUserName(), 
				null, null, orgUser.getUser().getName(), orgUser.getEmail());
	}
	
	@Override
	public OrganisationUserView getUserAndOrgDetail(Long id) throws NoResultFoundException, ApplicationException {
		OrganisationUser orgUser = getOrgUserById(id);
		
		OrganisationView orgView = organisationRestTemplate.getOrgById(orgUser.getOrganisationId());
		
		return new OrganisationUserView(orgUser.getId(), orgUser.getOrganisationId(), orgUser.getUser().getId(), orgUser.getUserName(), 
				orgUser.getPassword(), orgView.getName(), orgUser.getUser().getName(), orgUser.getEmail());
	}

	private OrganisationUser getOrgUserById(Long id) throws NoResultFoundException {
		Optional<OrganisationUser> optional = organisationUserRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		OrganisationUser orgUser =  optional.get();
		return orgUser;
	}
	
	@Override
	public void deActivateUser(Long orgUserId) throws NoResultFoundException {
		OrganisationUser orgUser = getOrgUserById(orgUserId);
		orgUser.setStatus(CommonConstants.STATUS_INACTIVE);
		organisationUserRepository.save(orgUser);
		
	}

	@Override
	public void deleteUser(Long orgUserId) throws NoResultFoundException {
		OrganisationUser orgUser = getOrgUserById(orgUserId);
		orgUser.setStatus(CommonConstants.SYSTEM_DELETED);
		organisationUserRepository.save(orgUser);
		
	}

	public void activateUser(Long orgUserId) throws NoResultFoundException {
		OrganisationUser orgUser = getOrgUserById(orgUserId);
		orgUser.setStatus(CommonConstants.STATUS_ACTIVE);
		organisationUserRepository.save(orgUser);
		
	}

	@Override
	public void changeUserPassword(String username, String newPassword) throws NoSuchMessageException, Exception {
		OrganisationUser orgUser = organisationUserRepository.findByUserName(username);
		if (!orgUser.getPassword().equals(EncryptionUtil.encrypt(newPassword))) {
			throw new PasswordNotMatchedException(messages.getMessage("error.password.not.match", null, null));
		}
		orgUser.setPassword(EncryptionUtil.encrypt(newPassword));
		organisationUserRepository.save(orgUser);
	}

	@Override
	public void resetPassword(String username, String password, String emailOTPToken) throws Exception {
		OrganisationUser orgUser = organisationUserRepository.findByUserName(username);
		if (orgUser == null) {
			throw new UserNotFoundException(messages.getMessage("error.user.not.found", null, null));
		}
		PasswordResetToken passwordToken = passwordResetTokenRepository.findByUsernameAndToken(username, emailOTPToken);
		if (passwordToken == null) {
			throw new InvalidTokenException(messages.getMessage("error.invalid.token", null, null));
		}
		orgUser.setPassword(EncryptionUtil.encrypt(password));
		organisationUserRepository.save(orgUser);
	}

	@Override
	public void createPasswordResetToken(String username) {
		OrganisationUser orgUser = organisationUserRepository.findByUserName(username);
		if (orgUser == null) {
			throw new UserNotFoundException(messages.getMessage("error.user.not.found", null, null));
		}
		String otp = CommonUtil.generateOTPForEmail(UserConstants.EMAIL_OTP_LENGTH);
		PasswordResetToken resetToken = new PasswordResetToken(otp, orgUser.getUserName());
		passwordResetTokenRepository.save(resetToken);
		try {
			userNotificationHelper.otpForPasswordReset(otp, orgUser.getEmail(),  orgUser.getName());
		} catch (Exception e) {
		}
	}

	@Override
	public OrganisationUserView getOrgUserByUserName(String username) {
		OrganisationUser orgUser = organisationUserRepository.findByUserName(username);
		return new OrganisationUserView(orgUser.getId(), orgUser.getOrganisationId(), orgUser.getUser().getId(), orgUser.getUserName(), 
				orgUser.getPassword(), null, orgUser.getUser().getName(), orgUser.getEmail());
	}
	
	private void sendWelcomeMail(OrganisationUser organisationUser) throws Exception {
		Map<String, Object> contentVariables = new HashMap<String, Object>();
		contentVariables.put("recipientName", organisationUser.getName());
		EmailView mailv = new EmailView(null, 
				organisationUser.getId(), 
				organisationUser.getEmail(), 
				CommonConstants.USER_WELCOME, 
				"Welcome To xrls", 
				CommonConstants.CONTENTTYPE_HTML, 
				null, 
				"welcomeEmail", 
				contentVariables);
		try {
			mailerProxy.sendMail(mailv);
		} catch (Exception e) {
		}
	}
}
