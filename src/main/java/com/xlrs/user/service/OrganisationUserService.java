package com.xlrs.user.service;

import org.springframework.context.NoSuchMessageException;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.user.view.OrganisationUserView;

public interface OrganisationUserService {

	public OrganisationUserView createOrgUser(OrganisationUserView organisationUserView) throws NoResultFoundException, Exception;;
	
	public OrganisationUserView getOrgUser(Long id) throws NoResultFoundException;

	public OrganisationUserView getUserAndOrgDetail(Long id) throws NoResultFoundException, ApplicationException;
	
	public void deActivateUser(Long orgUserId) throws NoResultFoundException;
	
	public void deleteUser(Long orgUserId) throws NoResultFoundException;
	
	public void activateUser(Long orgUserId) throws NoResultFoundException;

	public void changeUserPassword(String username, String newPassword) throws NoSuchMessageException, Exception;

	public void resetPassword(String username, String password, String emailOTPToken) throws Exception;

	public void createPasswordResetToken(String username);

	public OrganisationUserView getOrgUserByUserName(String username);

	
}
