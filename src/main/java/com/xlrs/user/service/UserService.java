package com.xlrs.user.service;

import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.user.entity.Users;
import com.xlrs.user.view.UserView;

public interface UserService {

	public UserView createUser(UserView createUserView) throws NoResultFoundException;
	
	public UserView getUser(Long id) throws NoResultFoundException;

	public Users getUserById(Long id) throws NoResultFoundException;
}
