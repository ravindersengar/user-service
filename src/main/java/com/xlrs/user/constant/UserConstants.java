package com.xlrs.user.constant;

public class UserConstants {
	
	public static final String SUPPORT_MAIL_ID 	= "tsf01@segelfisdche.com";
	
	public static final String TOKEN_INVALID 	= "invalidToken";
	public static final String TOKEN_EXPIRED 	= "expired";
	public static final String TOKEN_VALID 		= "valid";

	public static final Integer MOBILE_OTP_LENGTH 		= 6;
	public static final Integer EMAIL_OTP_LENGTH 		= 15;
	
	public static final String DEFAULT_USER_PREFERENCE = "DEFAULT_USER_PREFERENCE";
	
	public static final String SMS_MESSAGE_PREFIX 	= "Your OTP for LoveMaitri account is ";
	
	
	
}
