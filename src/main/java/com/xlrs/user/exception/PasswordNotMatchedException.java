package com.xlrs.user.exception;

import com.xlrs.commons.exception.ValidationException;

public final class PasswordNotMatchedException extends RuntimeException implements ValidationException{

    private static final long serialVersionUID = 5861310537366287163L;

    public PasswordNotMatchedException() {
        super();
    }

    public PasswordNotMatchedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PasswordNotMatchedException(final String message) {
        super(message);
    }

    public PasswordNotMatchedException(final Throwable cause) {
        super(cause);
    }

}
