package com.xlrs.user.exception;

import com.xlrs.commons.exception.ValidationException;

public final class UserNotFoundException extends RuntimeException implements ValidationException{

    private static final long serialVersionUID = 5861310537366287163L;

    public UserNotFoundException() {
        super();
    }

    public UserNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserNotFoundException(final String message) {
        super(message);
    }

    public UserNotFoundException(final Throwable cause) {
        super(cause);
    }

}
