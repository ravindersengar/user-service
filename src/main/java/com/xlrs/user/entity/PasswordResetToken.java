package com.xlrs.user.entity;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "usr_password_reset_token")
@Getter
@Setter
@NoArgsConstructor
public class PasswordResetToken {

	private static final int EXPIRATION = 30; //minutes

	@Id
	@Column(name = "username")
    private String username;
	
	@Column(name = "token")
	private String token;

	@Column(name = "expiry_date")
	private Date expiryDate;

	private Date calculateExpiryDate(final int expiryTimeInMinutes) {
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(new Date().getTime());
		cal.add(Calendar.MINUTE, expiryTimeInMinutes);
		return new Date(cal.getTime().getTime());
	}

	public PasswordResetToken(String token, String username) {
		super();
		this.token = token;
		this.username = username;
		this.expiryDate = calculateExpiryDate(EXPIRATION);
	}
}
