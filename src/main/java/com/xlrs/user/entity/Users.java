package com.xlrs.user.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.xlrs.commons.entity.AbstractEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class Users extends AbstractEntity{

	private static final long serialVersionUID = 1779829476650834122L;
	
	@NotEmpty(message = "{user.ssn.mandatory.feild.notempty}")
	private String ssn;
	@NotEmpty(message = "{user.name.mandatory.feild.notempty}")
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	@JsonManagedReference
	private List<OrganisationUser> organisationUsers;

}
