package com.xlrs.user.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.entity.AbstractEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class OrganisationUser extends AbstractEntity {
	
	private static final long serialVersionUID = -4743963813330055541L;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	@JsonBackReference
	private Users user;

	private Long organisationId;
	
	@NotEmpty(message = "{organisactionUser.username.mandatory.feild.notempty}")
	private String userName;
	
	@NotEmpty(message = "{organisactionUser.password.mandatory.feild.notempty}")
	private String password;
	
	@NotEmpty(message = "{organisactionUser.email.mandatory.feild.notempty}")
	private String email;
	
    @Size(min = 4, max = 20)
    private String userRoles;
	
    @Size(min = 4, max = 50)
    private String name;
}
