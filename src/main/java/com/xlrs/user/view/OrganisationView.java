package com.xlrs.user.view;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
@ToString
public class OrganisationView implements BaseView{

		private static final long serialVersionUID = 1L;
	
		@NotEmpty(message = "{organisation.name.mandatory.feild.notempty}")
		private String name;
		private Long id;
}
