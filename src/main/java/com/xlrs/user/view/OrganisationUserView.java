package com.xlrs.user.view;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class OrganisationUserView implements BaseView {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private Long organisationId;
	
	private Long userId;
	
	@NotEmpty(message = "{organisactionUser.username.mandatory.feild.notempty}")
	private String userName;
	
	@NotEmpty(message = "{organisactionUser.password.mandatory.feild.notempty}")
	private String password;
	
	private String organisationName;
	
	private String userRole;
	
	@NotEmpty(message = "{organisactionUser.email.mandatory.feild.notempty}")
	private String email;
}
