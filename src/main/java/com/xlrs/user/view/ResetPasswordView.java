package com.xlrs.user.view;

import com.xlrs.user.validation.ValidPassword;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ResetPasswordView {

    private String emailOTPToken;
    
    private String username;

    @ValidPassword
    private String password;
}
