package com.xlrs.user.helper;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.xlrs.commons.view.EmailView;
import com.xlrs.security.builder.RestServiceBuilder;
import com.xlrs.user.view.OrganisationUserView;

@Component
public class UserNotificationHelper {

	@Autowired
	private RestServiceBuilder<String> restServiceBuilder;

	@Value("${mailer.service.url}")
	private String notificationMailServiceURL;

	public void sendWelcomeMail(OrganisationUserView orgUserView, String userFullName) {
//		final String confirmationUrl = CommonConstants.serverContextPath + "/verifyEmail&username=" + userView.getUsername() + "&token=" + userView.getAuthToken();
		EmailView emailView = constructEmailView("Welcome to XLRS", "", "WELCOME_MAIL", orgUserView.getUserName(), "welcomeEmail", userFullName);
		sendMail(emailView);
	}

	public void otpForEmailVerification(String otpToken, String username, String userFullName) {
		EmailView view =  constructEmailView("Verify your Account", otpToken, "ACC_VERIFICATION_MAIL", username,  "emailVerification", userFullName);
		sendMail(view);
	}

	public void otpForPasswordReset(String otpToken, String username, String name) {
		EmailView view = constructEmailView("Reset Password", otpToken, "FORGET_PWD_RESET_MAIL", username,  "passwordReset", name);
		sendMail(view);
	}

	private EmailView constructEmailView(String subject, String otpToken, String emailType, String userName, String templateName, String name) {
		
		if(name==null || "".equals(name)) {
			name ="User";
		}
		EmailView emailView = new EmailView();
		emailView.setSubject(subject);
		emailView.setRequestType(emailType);

		Map<String, Object> contentVariables = new HashMap<>();
		contentVariables.put("otpToken", otpToken);
		contentVariables.put("recipientName", name);
		emailView.setContentVariables(contentVariables);
		emailView.setContentType("HTML");
		emailView.setRecipientMailId(userName);
		emailView.setTemplateName(templateName);
		return emailView;
	}
	
	private void sendMail(EmailView emailView) {
		try {
			restServiceBuilder.post(notificationMailServiceURL, emailView, String.class);
		} catch (Exception e) {
		}
	}

}
