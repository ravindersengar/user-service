package com.xlrs.user.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.xlrs.commons.view.ResponseView;

@FeignClient(name = "organisation-service")
public interface OrganisationProxy {
	@GetMapping("/organisation/{id}")
	public ResponseView getOrganisation(@PathVariable Long id) throws Exception;

}

