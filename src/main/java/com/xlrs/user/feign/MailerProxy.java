package com.xlrs.user.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.xlrs.commons.view.EmailView;

@FeignClient(name = "mailer-service")
public interface MailerProxy {
	
	@PostMapping("/mailer/email")
	public void sendMail(@RequestBody EmailView email) throws Exception ;

}

