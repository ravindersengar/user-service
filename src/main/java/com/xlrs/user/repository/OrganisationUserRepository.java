package com.xlrs.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xlrs.user.entity.OrganisationUser;

@Repository
public interface OrganisationUserRepository extends JpaRepository<OrganisationUser, Long>{

	List<OrganisationUser> findByOrganisationId(Long id);

	OrganisationUser findByUserName(String userName);

	OrganisationUser findByEmail(String email);
	
}
